#include "serialport.h"
#include <QFile>

SerialPort::SerialPort(QObject *parent) : QObject(parent)
{

}

void SerialPort::init(){
    sp = new QSerialPort(this);

}

void SerialPort::setComPort(QString comport){
    sp->setPortName(comport);
}

void SerialPort::setBaudrate(QSerialPort::BaudRate baudrate){
    sp->setBaudRate(baudrate);
}

void SerialPort::setDatabits(QSerialPort::DataBits databits){
    sp->setDataBits(databits);
}

void SerialPort::setParity(QSerialPort::Parity parity){
    sp->setParity(parity);
}

void SerialPort::setStopbits(QSerialPort::StopBits stopBits){
    sp->setStopBits(stopBits);
}

void SerialPort::connectToComPort(){

    if(sp->portName().isEmpty()){
        emit sendStatus("Com port name is empty");
        return;
    }

    if(sp->open(QIODevice::ReadWrite)){
        QByteArray pkt = "$$$\r\n";
        short attemp = 0;
        bool success = false;
        while (attemp < maxAttemp && !success){

            emit sendStatus(pkt.replace("\r\n","") + ". Attemp № " + QString::number(attemp));

            sp->write(pkt);


            sp->waitForReadyRead(10000);
            if(sp->bytesAvailable()){
                QByteArray rPkt = sp->readAll();

                if(rPkt.contains("CMD")){
                    emit connected();
                    emit sendStatus("Connected");
                    success = true;
                }
                else
                    emit sendStatus("Unrecognized commands " + rPkt);
            }
            else{

                emit sendStatus("Timeout. No answer");
            }

            if(!success){
                attemp++;                
                QThread::sleep(sleepS);
            }
        }
    }
    else{
        emit sendStatus("Failed to open " + sp->portName());
    }
}

void SerialPort::programm(){
    if(sp->isOpen()){
        if(sendToComport("SU,96\r\n")){
            emit sendStatus("Programming");
            emit sendSuccess("Baud Rate Set - Success");
            QThread::sleep(sleepS);
        }
        else
            return;

        if(sendToComport("SA,0\r\n")){
            emit sendStatus("Programming");
            emit sendSuccess("Authentication Set - Success");
            QThread::sleep(sleepS);
        }
        else
            return;

        if(sendToComport("R,1\r\n")){
            emit sendSuccess("Reboot Bluetooth - Success");
            emit sendStatus("Reboot");
        }
        else
            return;

        emit sendSuccess("Complete - Bluetooth has been reprogrammed. Please exit this program and turn Te Pari Scale off and back on again");

    }
}

inline bool SerialPort::parseReply(QByteArray reply){
    if(reply.contains("AOK") || reply.contains("Reboot")){
        return true;
    }
    else if(reply.contains("ERR")){
        emit sendStatus("Invalid syntax");
    }
    else
        emit sendStatus("Unrecognized commands");

    return false;
}

inline bool SerialPort::parseReply(QByteArray reply, QByteArray goodanswer){
    if(reply.contains(goodanswer) || reply.contains("Reboot")){
        return true;
    }
    else if(reply.contains("ERR")){
        emit sendStatus("Invalid syntax");
    }
    else
        emit sendStatus("Unrecognized commands");

    return false;
}

bool SerialPort::sendToComport(QByteArray pkt){
    short attemp = 0;
    bool success = false;
    while (attemp < maxAttemp && !success){

        sp->write(pkt);

        emit sendStatus(pkt.replace("\r\n","") + ". Attemp № " + QString::number(attemp));

        sp->waitForReadyRead(10000);
        if(sp->bytesAvailable()){
            QByteArray rPkt = sp->readAll();

            if(parseReply(rPkt)){
                success = true;
            }

        }
        else{
            emit sendStatus("Timeout. No answer");
        }

        if(!success){
            attemp++;
            QThread::sleep(sleepS);
        }
    }
    return success;

}
SerialPort::~SerialPort(){
}
