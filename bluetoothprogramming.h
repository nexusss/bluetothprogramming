#ifndef BLUETOOTHPROGRAMMING_H
#define BLUETOOTHPROGRAMMING_H

#include <QObject>
#include <QSerialPortInfo>
#include <QMap>
#include <QThread>
#include "serialport.h"

class BluetoothProgramming : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QStringList spList READ spList  NOTIFY splistChanged)
    Q_PROPERTY(QStringList baudrateList READ baudrateList NOTIFY baudrateListChanged)
    Q_PROPERTY(QStringList databitsList READ databitsList NOTIFY databitsListChanged)
    Q_PROPERTY(QStringList parityList READ parityList NOTIFY parityListChanged)
    Q_PROPERTY(QStringList stopBitsList READ stopBitsList NOTIFY stopBitsListChanged)


    QMap <QString, QString> mspList;
    QMap <QString, QSerialPort::BaudRate> mbaudrateList;
    QMap <QString, QSerialPort::DataBits> mdatabitsList;
    QMap <QString, QSerialPort::Parity> mparityList;
    QMap <QString, QSerialPort::StopBits> mstopBitsList;

    SerialPort *sp;
    QThread spThread;
public:
    explicit BluetoothProgramming(QObject *parent = 0);
    ~BluetoothProgramming();
    QStringList spList();
    QStringList baudrateList();
    QStringList databitsList();
    QStringList parityList();
    QStringList stopBitsList();
signals:
    void splistChanged();
    void baudrateListChanged();
    void databitsListChanged();
    void parityListChanged();
    void stopBitsListChanged();
    void sendStatus(QString status);
    void sendSuccess(QString status);
    void connected();
public slots:
    void setComPort(QString comport);
    void setBaudrate(QString baudrate);
    void setDatabits(QString databits);
    void setParity(QString parity);
    void setStopbits(QString stopBits);

    void connectToComPort();
    void programm();
};

#endif // BLUETOOTHPROGRAMMING_H
