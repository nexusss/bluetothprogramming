#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QObject>
#include <QSerialPort>
#include <QThread>

class SerialPort : public QObject
{
    Q_OBJECT
    QSerialPort *sp;

    const short maxAttemp = 4;
    const short sleepS = 3;

    bool sendToComport(QByteArray pkt);
    inline bool parseReply(QByteArray reply);
    inline bool parseReply(QByteArray reply,QByteArray goodanswer);
public:
    explicit SerialPort(QObject *parent = 0);
    ~SerialPort();

signals:
    void sendStatus(QString status);
    void sendSuccess(QString status);
    void connected();
public slots:
    void init();
    void setComPort(QString comport);
    void setBaudrate(QSerialPort::BaudRate baudrate);
    void setDatabits(QSerialPort::DataBits databits);
    void setParity(QSerialPort::Parity parity);
    void setStopbits(QSerialPort::StopBits stopBits);

    void connectToComPort();
    void programm();

};

#endif // SERIALPORT_H
