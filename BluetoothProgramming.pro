QT += qml quick serialport

CONFIG += c++11

SOURCES += main.cpp \
    bluetoothprogramming.cpp \
    serialport.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

RC_ICONS = TEPARI_RGB.ico

HEADERS += \
    bluetoothprogramming.h \
    serialport.h
