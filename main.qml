import QtQuick 2.7
import QtQuick.Window 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import MyClass.BuetoothProgramming 1.0

ApplicationWindow {
    visible: true
    width: 800
    height: 480
    title: qsTr("Racewell Bluetooth Programmer")

    BluetoothProgramming{
        id: bluetooth
        onSendStatus: statustext.text = status
        onConnected: {
            confButton.enabled = true
            connectButton.enabled = false
        }
        onSendSuccess: succesTextArea.append(status )
    }
    ComboBox {
        id: spCombobox
        x: 124
        y: 20
        width: 375
        height: 40
        model: bluetooth.spList
        onCurrentTextChanged: bluetooth.setComPort(currentText)
        enabled: connectButton.enabled
    }

    Text {
        x: 9
        y: 28
        text: qsTr("Serial port")
        font.pixelSize: 20
    }

    Text {
        x: 295
        y: 89
        text: qsTr("Data bits")
        font.pixelSize: 20
    }

    ComboBox {
        id: dbComboBox
        x: 385
        y: 81
        width: 114
        height: 40
        model: bluetooth.databitsList
        onCurrentTextChanged: bluetooth.setDatabits(currentText)
        Component.onCompleted: currentIndex = find("8")
        enabled: connectButton.enabled
    }

    Text {
        x: 9
        y: 89
        text: qsTr("Baudrate")
        font.pixelSize: 20
    }

    ComboBox {
        id: brComboBox
        x: 124
        y: 81
        width: 156
        height: 40
        model: bluetooth.baudrateList
        onCurrentTextChanged: bluetooth.setBaudrate(currentText)
        Component.onCompleted: currentIndex = find("9600")
        enabled: connectButton.enabled
    }

    Text {
        x: 9
        y: 151
        text: qsTr("Parity")
        font.pixelSize: 20
    }

    ComboBox {
        id: parityComboBox
        x: 124
        y: 143
        width: 156
        height: 40
        model: bluetooth.parityList
        onCurrentTextChanged: bluetooth.setParity(currentText)
        Component.onCompleted: currentIndex = find("NoParity")
        enabled: connectButton.enabled
    }

    Text {
        x: 297
        y: 151
        text: qsTr("Stop bits")
        font.pixelSize: 20
    }

    ComboBox {
        id: sbComboBox
        x: 385
        y: 143
        width: 114
        height: 40
        model: bluetooth.stopBitsList
        onCurrentTextChanged: bluetooth.setStopbits(currentText)
        Component.onCompleted: currentIndex = find("One Stop")
        enabled: connectButton.enabled
    }

    Button {
        id: connectButton
        x: 9
        y: 204
        text: qsTr("Connect")
        onClicked: bluetooth.connectToComPort()
    }

    Button {
        id: confButton
        x: 115
        y: 204
        text: qsTr("Configure")
        onClicked: bluetooth.programm()
        enabled: false
    }

    Text {
        x: 9
        y: 263
        text: qsTr("Status:")
        font.pixelSize: 20
    }

    Text {
        id: statustext
        x: 86
        y: 263
        width: 413
        height: 24
        text: qsTr("")
        font.pixelSize: 20
    }

    TextArea {
        id: succesTextArea
        x: 9
        y: 293
        width: 490
        height: 167
        wrapMode: Text.WordWrap
        textFormat: Text.AutoText
        font.pointSize: 10
    }

    TextArea {
        x: 512
        y: 20
        width: 282
        height: 450
        text: qsTr("To configure Bluetooth in your Te Pari weigh scales please follow instructions below:<br><br>Connect the scales COM 1 to the computer using the supplied cables<br><br>Turn the scales on<br><br>Press SETUP/WEIGH and arrow to SETUP PAGE 2<br><br>Ensure that bluetooth power is ON<br><br>Press B on the scales keyboard to enter bluetooth configuration mode<br><br>Select the com port from the list below. (Normally will be called USB serial or similar)\nThen click the connect button on this screen.<br><br>Once connected click the configure button and wait for programming finish.")
        textFormat: Text.RichText
        font.pointSize: 11
        wrapMode: Text.WordWrap
    }
}
