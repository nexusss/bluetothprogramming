#include "bluetoothprogramming.h"

BluetoothProgramming::BluetoothProgramming(QObject *parent) : QObject(parent)

{
    sp = new SerialPort();
    sp->moveToThread(&spThread);
    connect(&spThread,&QThread::started,sp,&SerialPort::init);
    connect(&spThread,&QThread::destroyed,sp,&SerialPort::deleteLater);
    connect(sp,&SerialPort::connected,this,&BluetoothProgramming::connected);
    connect(sp,&SerialPort::sendStatus,this,&BluetoothProgramming::sendStatus);
    connect(sp,&SerialPort::sendSuccess,this,&BluetoothProgramming::sendSuccess);
    spThread.start();

    QList<QSerialPortInfo> spAvailable = QSerialPortInfo::availablePorts();
    foreach (QSerialPortInfo sp, spAvailable) {
        mspList.insert(sp.portName() + ": " + sp.description(),sp.portName());
    }

    mbaudrateList.insert("1200",QSerialPort::Baud1200);
    mbaudrateList.insert("2400",QSerialPort::Baud2400);
    mbaudrateList.insert("4800",QSerialPort::Baud4800);
    mbaudrateList.insert("9600",QSerialPort::Baud9600);
    mbaudrateList.insert("19200",QSerialPort::Baud19200);
    mbaudrateList.insert("38400",QSerialPort::Baud38400);
    mbaudrateList.insert("57600",QSerialPort::Baud57600);
    mbaudrateList.insert("115200",QSerialPort::Baud115200);

    mdatabitsList.insert("5",QSerialPort::Data5);
    mdatabitsList.insert("6",QSerialPort::Data6);
    mdatabitsList.insert("7",QSerialPort::Data7);
    mdatabitsList.insert("8",QSerialPort::Data8);

    mparityList.insert("NoParity",QSerialPort::NoParity);
    mparityList.insert("EvenParity",QSerialPort::EvenParity);
    mparityList.insert("OddParity",QSerialPort::OddParity);
    mparityList.insert("SpaceParity",QSerialPort::SpaceParity);
    mparityList.insert("MarkParity",QSerialPort::MarkParity);

    mstopBitsList.insert("One Stop",QSerialPort::OneStop);
    mstopBitsList.insert("One And Half Stop",QSerialPort::OneAndHalfStop);
    mstopBitsList.insert("Two Stop",QSerialPort::TwoStop);

    qRegisterMetaType<QSerialPort::BaudRate>("QSerialPort::BaudRate");
    qRegisterMetaType<QSerialPort::DataBits>("QSerialPort::DataBits");
    qRegisterMetaType<QSerialPort::Parity>("QSerialPort::Parity");
    qRegisterMetaType<QSerialPort::StopBits>("QSerialPort::StopBits");
}

QStringList BluetoothProgramming::spList(){
    QStringList list;
    foreach (QString str, mspList.keys()) {
        list << str;
    }
    return list;
}

QStringList BluetoothProgramming::baudrateList(){
    QStringList list;
    foreach (QString str, mbaudrateList.keys()) {
        list << str;
    }
    return list;
}

QStringList BluetoothProgramming::databitsList(){
    QStringList list;
    foreach (QString str, mdatabitsList.keys()) {
        list << str;
    }
    return list;
}

QStringList BluetoothProgramming::parityList(){
    QStringList list;
    foreach (QString str, mparityList.keys()) {
        list << str;
    }
    return list;
}

QStringList BluetoothProgramming::stopBitsList(){
    QStringList list;
    foreach (QString str, mstopBitsList.keys()) {
        list << str;
    }
    return list;
}

void BluetoothProgramming::setComPort(QString comport){
    QMetaObject::invokeMethod(sp,"setComPort",Q_ARG(QString,mspList.value(comport)));
}

void BluetoothProgramming::setBaudrate(QString baudrate){
    QMetaObject::invokeMethod(sp,"setBaudrate",Q_ARG(QSerialPort::BaudRate,mbaudrateList.value(baudrate)));
}

void BluetoothProgramming::setDatabits(QString databits){
    QMetaObject::invokeMethod(sp,"setDatabits",Q_ARG(QSerialPort::DataBits,mdatabitsList.value(databits)));
}

void BluetoothProgramming::setParity(QString parity){
    QMetaObject::invokeMethod(sp,"setParity",Q_ARG(QSerialPort::Parity,mparityList.value(parity)));
}

void BluetoothProgramming::setStopbits(QString stopBits){
    QMetaObject::invokeMethod(sp,"setStopbits",Q_ARG(QSerialPort::StopBits,mstopBitsList.value(stopBits)));
}

void BluetoothProgramming::connectToComPort(){
    QMetaObject::invokeMethod(sp,"connectToComPort");
}

void BluetoothProgramming::programm(){
    QMetaObject::invokeMethod(sp,"programm");
}

BluetoothProgramming::~BluetoothProgramming(){
    spThread.quit();
    spThread.wait();
}
