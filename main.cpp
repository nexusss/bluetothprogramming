#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "bluetoothprogramming.h"
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qmlRegisterType<BluetoothProgramming>("MyClass.BuetoothProgramming", 1, 0, "BluetoothProgramming");


    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));

    return app.exec();
}
